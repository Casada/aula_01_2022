// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
      apiKey: "AIzaSyBWDYq3FtQNZ3CPb4nd9_pgOgZu7HMq9Sc",
      authDomain: "controle-ifsp-kawan.firebaseapp.com",
      projectId: "controle-ifsp-kawan",
      storageBucket: "controle-ifsp-kawan.appspot.com",
      messagingSenderId: "904580786395",
      appId: "1:904580786395:web:f0b274a677f7291e9644fe",
      measurementId: "G-LG757V79YT"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
